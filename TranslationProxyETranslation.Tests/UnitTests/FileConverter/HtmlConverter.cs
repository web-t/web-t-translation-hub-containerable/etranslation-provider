﻿using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace TextTranslationService.Tests.UnitTests.FileConverter
{
    public class HtmlConverter
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                var testCase = new object[]
                    {
                        new object[]
                        {
                            "Segment 1",
                            "Data <a> test </a>",
                            "",
                            "<div></div>",
                            "<br>"
                        }
                    };
                return new List<object[]> { testCase };
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void ConverterOutputEquivalentToInputData(object[] inputData)
        {
            var testData = inputData.Cast<string>();
            var converter = new Tilde.TranslationProxyETranslation.Converters.File.HtmlConverter(testData);

            var base64 = converter.ToBase64();

            var fromBase64 = converter.FromBase64(base64);

            inputData.Should().BeEquivalentTo(fromBase64);
        }
    }
}
