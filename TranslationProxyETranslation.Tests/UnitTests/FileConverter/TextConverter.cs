﻿using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace TextTranslationService.Tests.UnitTests.FileConverter
{
    public class TextConverter
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                var testCase = new object[]
                    {
                        new object[]
                        {
                            "Segment \n\n\n\n 1",
                            "Data <a> test </a>",
                            "",
                            "<div /> << >> "
                        }
                    };
                return new List<object[]> { testCase };
            }
        }

        [Theory]
        [MemberData(nameof(TestData))]
        public void ConverterOutputEquivalentToInputData(object[] inputData)
        {
            var testData = inputData.Cast<string>();
            var converter = new Tilde.TranslationProxyETranslation.Converters.File.TxtConverter(testData);

            var base64 = converter.ToBase64();

            var fromBase64 = converter.FromBase64(base64);

            var inputNormalized = inputData.Select(item => ((string)item).Replace("\n", ""));
            inputNormalized.Should().BeEquivalentTo(fromBase64);
        }

        /// <summary>
        /// Support that MT engine can add extra newline at the end. Converter should remove it to match input segment count
        /// </summary>
        [Fact]
        public void ConverterOutputTrimEmptyOutputToMatchInputSize()
        {
            var testData = new[]
            {
                "foo",
                "bar"
            };

            var testDataWithEmptyTrailing = new[]
            {
                "foo",
                "bar",
                ""
            };

            var converter = new Tilde.TranslationProxyETranslation.Converters.File.TxtConverter(testData);
            var tmpConverter = new Tilde.TranslationProxyETranslation.Converters.File.TxtConverter(testDataWithEmptyTrailing);

            var base64 = tmpConverter.ToBase64();

            var fromBase64 = converter.FromBase64(base64);

            testData.Should().BeEquivalentTo(fromBase64);
        }
    }
}
