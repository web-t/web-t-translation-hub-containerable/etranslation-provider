# Translation Proxy eTranslation

### Architecture for eTransaltion

1. Send a request to Translation Proxy
2. Send a request to eTranslation external service
3. Send async callback random Translation Proxy instance 
4. Publish message to Redis, so other services can see eTranslation callback
5. Service Subscribes to Redis to receive all eTranslation callbacks If service instance has registered {Request id}, then it returns message to client otherwise discards

```
 ----------------------------------    
|                                  | | ← ← ← ← ← ← ← ←
|  Translation Proxy eTranslation  | |                 ↑   5. Service Subscribes to Redis to receive all eTranslation callbacks
|                                  | |  → → → → → →    ↑       If service instance has registered {Request id}, then
 ----------------------------------| ← ← ← ←       ↓   ↑           it returns message to client otherwise discards.
   -------------------------------          ↑      ↓   ↑
                                            ↑      ↓   ↑
        ↓                                   ↑      ↓   ↑
        ↓ 1. Request text segment           ↑      ↓   ↑
        ↓                                   ↑      ↓   ↑
                ↑                           ↑      ↓   ↑
                ↑ 2. Returns {Request id}   ↑      ↓   ↑
                ↑                           ↑      ↓---↑-- 4. Publish message to Redis, so other services
                                            ↑      ↓   ↑       can see eTranslation callback
 -------------------------------            ↑      ↓   ↑
|                               | |         ↑      ↓   ↑
|    eTransaltion [external]    | | → → → →        ↓   ↑
|                               | |                ↓   ↑   3. Send async callback response to random instance with:
 -------------------------------  |                ↓   ↑   {Request id} + {Translated text segment}
   -------------------------------                 ↓   ↑
                                                   ↓   ↑
                                                   ↓   ↑
 -------------------------------                   ↓   ↑
|                               | ← ← ← ← ← ← ← ← ←    ↑
|    Redis (Pub/Sub)            |                      ↑
|                               | → → → → → → → → → → →
 -------------------------------
```

# Monitor

## Healthcheck probes

https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/

Startup probe / Readiness probe:

`/health/ready`

Liveness probe:

`/health/live`


# Test

Using docker compose
```
docker-compose up --build
```

Open Swagger
```
http://localhost:5005/swagger/index.html
```