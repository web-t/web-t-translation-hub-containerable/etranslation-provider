﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace Tilde.TranslationProxyETranslation.Extensions
{
    public static class CorsExtensions
    {
        private static readonly string DevelopmentCorsPolicy = "development-policy";

        /// <summary>
        /// Add cors policies
        /// </summary>
        /// <param name="services"> Service collection </param>
        /// <returns></returns>
        public static IServiceCollection AddCorsPolicies(this IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(
                    name: DevelopmentCorsPolicy,
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200").AllowAnyHeader();
                    }
                );
            });

            return services;
        }

        /// <summary>
        /// Use cors policies
        /// </summary>
        /// <param name="app"> Application builder </param>
        /// <returns></returns>
        public static IApplicationBuilder UseCorsPolicies(this IApplicationBuilder app)
        {
            app.UseCors(DevelopmentCorsPolicy);

            return app;
        }
    }
}
