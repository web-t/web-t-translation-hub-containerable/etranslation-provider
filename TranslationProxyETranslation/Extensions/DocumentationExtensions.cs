﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.IO;
using Tilde.TranslationProxyETranslation.Models.Configuration;

namespace Tilde.TranslationProxyETranslation.Extensions
{
    public static class DocumentationExtensions
    {
        /// <summary>
        /// Add swagger documentation
        /// </summary>
        /// <param name="services"> Service collection </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>
        public static IServiceCollection AddDocumentation(this IServiceCollection services, ConfigurationSettings configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = nameof(Tilde.TranslationProxyETranslation), Version = "v1" });

                c.AddSecurityDefinition("BasicAuth", new OpenApiSecurityScheme
                {
                    Name = "BasicAuth",
                    Description = "Enter your credentials",
                    Type = SecuritySchemeType.Http,
                    Scheme = "basic",
                    In = ParameterLocation.Header
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "BasicAuth"
                            }
                        },
                        Array.Empty<string>()
                    }
                });

                c.EnableAnnotations();

                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{nameof(Tilde)}.{nameof(TranslationProxyETranslation)}.xml"));

                c.AddServer(new OpenApiServer()
                {
                    Url = configuration.PublicUri.ToString()
                });
            });

            return services;
        }

        /// <summary>
        /// Use documentation
        /// </summary>
        /// <param name="app"> Application builder </param>
        /// <param name="configuration"> Configuration settings </param>
        /// <returns></returns>
        public static IApplicationBuilder UseDocumentation(this IApplicationBuilder app, ConfigurationSettings configuration)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint(configuration.PublicUri + "/swagger/v1/swagger.json", $"{nameof(Tilde.TranslationProxyETranslation)} v1"));

            return app;
        }
    }
}
