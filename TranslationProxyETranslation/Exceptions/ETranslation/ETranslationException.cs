﻿using System;

namespace Tilde.TranslationProxyETranslation.Exceptions.ETranslation
{
    public class ETranslationException : Exception
    {
        public ETranslationException(int registrationError) : base($"eTranslation request registration error:'{registrationError}'")
        {

        }

        public ETranslationException(string requestId, int errorCode) : this(requestId, errorCode, null)
        {

        }

        public ETranslationException(string requestId, int errorCode, string errorMessage) : base($"eTranslation request '{requestId}' error: '{errorCode}' message:'{errorMessage}'")
        {

        }
    }
}
