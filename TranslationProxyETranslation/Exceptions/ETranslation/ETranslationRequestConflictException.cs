﻿using System;

namespace Tilde.TranslationProxyETranslation.Exceptions.ETranslation
{
    public class ETranslationRequestConflictException : Exception
    {
        public ETranslationRequestConflictException(string requestId) : base($"eTransaltion request with same id '{requestId}' already exists")
        {

        }
    }
}
