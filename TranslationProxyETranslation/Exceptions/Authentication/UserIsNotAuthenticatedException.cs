﻿using System;

namespace Tilde.TranslationProxyETranslation.Exceptions.Authentication
{
    public class UserIsNotAuthenticatedException : Exception
    {
        public UserIsNotAuthenticatedException() :
            base($"User is not authenticated")
        {

        }
        public UserIsNotAuthenticatedException(string message) :
            base($"eTranslation service response: {message}")
        {

        }
    }
}
