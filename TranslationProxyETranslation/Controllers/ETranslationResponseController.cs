﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Tilde.TranslationProxyETranslation.Interfaces.Services;
using Tilde.TranslationProxyETranslation.Models.Translators.ETranslation;

namespace Tilde.TranslationProxyETranslation.Controllers
{
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("translate/text/e-translation")]
    public class ETranslationResponseController : Controller
    {
        private readonly IETranslationService _eTranslationService;
        public ETranslationResponseController(
            IETranslationService eTranslationService
        )
        {
            _eTranslationService = eTranslationService;
        }

        /// <summary>
        /// Returns eTranslation result
        /// </summary>
        /// <param name="model"> eTranslation response </param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromQuery] ResponseModel model)
        {
            if (!Request.Body.CanSeek)
            {
                // We only do this if the stream isn't *already* seekable,
                // as EnableBuffering will create a new stream instance
                // each time it's called
                Request.EnableBuffering();
            }

            Request.Body.Position = 0;

            using var reader = new StreamReader(Request.Body, Encoding.UTF8);

            // contentsBase64 example: PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPGZyYWdtZW50PnRla3N0czwvZnJhZ21lbnQ+
            var contentsBase64 = await reader.ReadToEndAsync().ConfigureAwait(false);

            await _eTranslationService.TranslateCallback(model.RequestId, model.RequestSecret, contentsBase64, null, null);

            return Ok();
        }

        /// <summary>
        /// Returns error from eTranslation
        /// </summary>
        /// <param name="model"> eTranslation error response </param>
        /// <returns></returns>
        [HttpPost]
        [Route("error")]
        public async Task<IActionResult> PostError([FromForm] ResponseErrorModel model)
        {
            await _eTranslationService.TranslateCallback(model.RequestId, model.RequestSecret, null, model.ErrorCode, model.ErrorMessage);

            return Ok();
        }

    }
}
