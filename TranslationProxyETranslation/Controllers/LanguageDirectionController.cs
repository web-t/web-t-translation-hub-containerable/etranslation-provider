﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Enums.Error;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.TranslationProxyETranslation.Exceptions.Authentication;
using Tilde.TranslationProxyETranslation.Interfaces.Services;

namespace Tilde.TranslationProxyETranslation.Controllers
{
    [ApiController]
    [Route("translate/language-directions")]
    public class LanguageDirectionController : BaseController
    {
        private readonly ILogger _logger;
        private readonly IETranslationService _eTranslationService;

        public LanguageDirectionController(
            ILogger<LanguageDirectionController> logger,
            IETranslationService eTranslationService
        )
        {
            _logger = logger;
            _eTranslationService = eTranslationService;
        }

        /// <summary>
        /// Lists available languages and domains
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("")]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(LanguageDirectionsResponse), Description = "")]
        public async Task<ActionResult<LanguageDirectionsResponse>> List()
        {
            try
            {
                var authorization = Request.Headers.SingleOrDefault(h => h.Key.Equals("Authorization", StringComparison.InvariantCultureIgnoreCase)).Value.SingleOrDefault();

                var authenticationModel = Helpers.BasicAuthenticationHelper.GetCredentials(authorization);

                LanguageDirectionsResponse systems = await _eTranslationService.FetchDomains(authenticationModel);

                return Ok(systems);
            }
            catch (UserIsNotAuthenticatedException ex)
            {
                _logger.LogError(ex, "User is not authenticated");

                return FormatAPIError(HttpStatusCode.Unauthorized, ErrorSubCode.GatewayRequestValidation);
            }
        }
    }
}
