﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Enums.Error;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.Errors;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.TranslationProxyETranslation.Exceptions.Authentication;
using Tilde.TranslationProxyETranslation.Exceptions.ETranslation;
using Tilde.TranslationProxyETranslation.Interfaces.Services;

namespace Tilde.TranslationProxyETranslation.Controllers
{
    /// <summary>
    /// API for text translation
    /// </summary>
    [ApiController]
    [Route("translate/text")]
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class TextController : BaseController
    {
        private readonly ILogger<TextController> _logger;
        private readonly IMapper _mapper;
        private readonly IETranslationService _eTranslationService;


        public TextController(
            ILogger<TextController> logger,
            IMapper mapper,
            IETranslationService eTranslationService
        )
        {
            _logger = logger;
            _mapper = mapper;
            _eTranslationService = eTranslationService;
        }

        /// <summary>
        /// TranslateSegment text
        /// </summary>
        /// <param name="request"> Translation request </param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse((int)HttpStatusCode.OK, Type = typeof(Translation))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, Description = "Missing or incorrect parameters", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError, Description = "An unexpected error occured", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.GatewayTimeout, Description = "Request timed out", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.NotFound, Description = "Language direction is not found", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.TooManyRequests, Description = "Too many requests", Type = typeof(APIError))]
        [SwaggerResponse((int)HttpStatusCode.RequestEntityTooLarge, Description = "Maximum text size limit reached for the request", Type = typeof(APIError))]
        public async Task<ActionResult<Translation>> GetTranslation(TranslationRequest request)
        {
            try
            {
                var authorization = Request.Headers.SingleOrDefault(h => h.Key.Equals("Authorization", StringComparison.InvariantCultureIgnoreCase)).Value.SingleOrDefault();

                var authenticationModel = Helpers.BasicAuthenticationHelper.GetCredentials(authorization);

                var response = await _eTranslationService.Translate(request.Text, request.SourceLanguage, request.TargetLanguage, request.Domain, authenticationModel);

                return Ok(response);
            }
            catch (LanguageDirectionNotFoundException ex)
            {
                _logger.LogError(ex, "Language is invalid");

                return FormatAPIError(HttpStatusCode.BadRequest, ErrorSubCode.GatewayLanguageDirectionNotFound);
            }
            catch (ETranslationException ex)
            {
                _logger.LogError("eTranslation failed: {Message}", ex.Message);

                return FormatAPIError(
                    HttpStatusCode.InternalServerError,
                    ErrorSubCode.WorkerTranslationGeneric,
                    message: ex.Message,
                    messageStatusCode: null
                );
            }
            catch (ETranslationConcurrencyQuotaExceeded)
            {
                return FormatAPIError(
                    HttpStatusCode.TooManyRequests,
                    ErrorSubCode.WorkerTranslationGeneric,
                    message: "",
                    messageStatusCode: null
                );
            }
            catch (TranslationTimeoutException ex)
            {
                _logger.LogError(ex, "Translation timed out");

                return FormatAPIError(HttpStatusCode.GatewayTimeout, ErrorSubCode.GatewayTranslationTimedOut);
            }
            catch (UserIsNotAuthenticatedException ex)
            {
                _logger.LogError(ex, "User is not authenticated");

                return FormatAPIError(HttpStatusCode.Unauthorized, ErrorSubCode.GatewayRequestValidation);
            }
        }
    }
}
