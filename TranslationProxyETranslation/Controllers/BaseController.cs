﻿using EnumsNET;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using Tilde.EMW.Contracts.Enums.Error;
using Tilde.EMW.Contracts.Models.Common.Errors;

namespace Tilde.TranslationProxyETranslation.Controllers
{
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Returns API error in the unified format, providing error code and message
        /// </summary>
        /// <param name="status"> HTTP status code </param>
        /// <param name="subcode"> Error sub code </param>
        /// <param name="messageStatusCode"> Message HTTP status code </param>
        /// <param name="message"> Message </param>
        /// <returns></returns>
        protected ObjectResult FormatAPIError(HttpStatusCode status, ErrorSubCode subcode, HttpStatusCode? messageStatusCode = null, string message = null)
        {
            return StatusCode(
                (int)status,
                new APIError()
                {
                    Error = new Error()
                    {
                        Code = (int)(messageStatusCode ?? status) * 1000 + (int)subcode,
                        Message = message ?? subcode.AsString(EnumFormat.Description)
                    }
                }
            );
        }
    }
}
