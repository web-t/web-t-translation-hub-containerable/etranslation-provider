﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tilde.TranslationProxyETranslation.Models.DTO.ETranslation
{
    public class Domain
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("languagePairs")]
        public List<string> LanguagePairs { get; set; }
    }
}
