﻿using System;
using System.Collections.Generic;

namespace Tilde.TranslationProxyETranslation.Models.Configuration.Services
{
    /// <summary>
    /// ETranslation service model
    /// </summary>
    public class ETranslation
    {
        public string UserName { get; init; }
        public string Password { get; init; }
        public List<string> EmailRecipients { get; init; } = new List<string>();
        public TimeSpan Timeout { get; init; } = TimeSpan.FromSeconds(60);
    }
}
