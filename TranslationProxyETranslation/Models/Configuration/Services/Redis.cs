﻿namespace Tilde.TranslationProxyETranslation.Models.Configuration.Services
{
    public record Redis
    {
        /// <summary>
        /// Redis connection URI
        /// </summary>
        public string Uri { get; init; }
    }
}
