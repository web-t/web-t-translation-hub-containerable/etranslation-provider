﻿using System;

namespace Tilde.TranslationProxyETranslation.Models.Configuration
{
    public record ConfigurationSettings
    {
        /// <summary>
        /// Public uri for eTranslation service to know where to send translation response back
        /// </summary>
        public Uri PublicUri { get; init; }
    }
}
