﻿namespace Tilde.TranslationProxyETranslation.Models.Configuration
{
    public class Authentication
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
