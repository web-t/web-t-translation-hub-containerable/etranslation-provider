﻿namespace Tilde.TranslationProxyETranslation.Models.Configuration
{
    public record ConfigurationServices
    {
        public Services.ETranslation ETranslation { get; init; }
        public Services.Redis Redis { get; init; }
    }
}
