﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public record ResponseModel
    {
        [FromQuery(Name = "request-id")]
        [MaxLength(50)]
        public string RequestId { get; init; }

        [FromQuery(Name = "target-language")]
        [MaxLength(50)]
        public string TargetLanguage { get; init; }

        [FromQuery(Name = "external-reference")]
        [MaxLength(300)]
        public string RequestSecret { get; init; }
    }
}
