﻿using System.Text.Json.Serialization;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public class CallerInformation
    {
        [JsonPropertyName("application")]
        public string Application { get; set; }
        [JsonPropertyName("username")]
        public string Username { get; set; }
    }
}
