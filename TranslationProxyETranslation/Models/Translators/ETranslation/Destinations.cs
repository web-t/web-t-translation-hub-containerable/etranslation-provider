﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public class Destinations
    {
        [JsonPropertyName("httpDestinations")]
        public List<string> HTTPDestinations { get; set; }

        [JsonPropertyName("ftpDestinations")]
        public List<string> FTPDestinations { get; set; }

        [JsonPropertyName("sftpDestinations")]
        public List<string> SFTPDestinations { get; set; }

        [JsonPropertyName("emailDestinations")]
        public List<string> EmailDestinations { get; set; }
    }
}
