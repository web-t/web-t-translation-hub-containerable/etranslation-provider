﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public class TranslationRequest
    {
        [JsonPropertyName("externalReference")]
        public string ExternalReference { get; set; }

        [JsonPropertyName("callerInformation")]
        public CallerInformation CallerInfo { get; set; }

        [JsonPropertyName("documentToTranslateBase64")]
        public DocumentBase64 DocumentToTranslateBase64 { get; set; }

        [JsonPropertyName("sourceLanguage")]
        public string SourceLanguage { get; set; }

        [JsonPropertyName("targetLanguages")]
        public List<string> TargetLanguages { get; set; }

        [JsonPropertyName("domain")]
        public string Domain { get; set; }

        [JsonPropertyName("requesterCallback")]
        public string RequesterCallback { get; set; }

        [JsonPropertyName("errorCallback")]
        public string ErrorCallback { get; set; }
        public Destinations Destinations { get; set; }

    }
}
