﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public record ResponseErrorModel
    {
        [FromForm(Name = "request-id")]
        [MaxLength(50)]
        public string RequestId { get; init; }

        [FromForm(Name = "error-code")]
        [MaxLength(50)]
        public string ErrorCode { get; init; }

        [FromForm(Name = "error-message")]
        [MaxLength(1000)]
        public string ErrorMessage { get; init; }

        [FromForm(Name = "external-reference")]
        [MaxLength(300)]
        public string RequestSecret { get; init; }

        [FromForm(Name = "target-languages")]
        public List<string> TargetLanguage { get; init; }
    }
}
