﻿using System.Text.Json.Serialization;

namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public class DocumentBase64
    {
        [JsonPropertyName("content")]
        public string Content { get; set; }

        [JsonPropertyName("format")]
        public string Format { get; set; }


        [JsonPropertyName("fileName")]
        public string FileName { get; set; }
    }
}
