﻿namespace Tilde.TranslationProxyETranslation.Models.Translators.ETranslation
{
    public class RedisMessage
    {
        public string RequestId { get; init; }
        public string RequestSecret { get; init; }
        public string ContentsBase64 { get; init; }
        public int? ErrorCode { get; init; }
        public string ErrorMessage { get; init; }
    }
}
