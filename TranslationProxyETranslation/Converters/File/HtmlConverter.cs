﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Interfaces.Converters;

namespace Tilde.TranslationProxyETranslation.Converters.File
{
    public class HtmlConverter : IFileConverter
    {
        private readonly IEnumerable<string> originalData;
        public bool Supported { get; private set; } = true;

        public HtmlConverter(IEnumerable<string> data)
        {
            originalData = data;
        }

        public IEnumerable<string> FromBase64(string data)
        {
            var bytes = Convert.FromBase64String(data);
            var html = Encoding.UTF8.GetString(bytes);

            var document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(html);

            return document.DocumentNode.ChildNodes
                .Select(item => item.InnerHtml);
        }

        public ETranslationDocumentFormat GetCEFDocFormat()
        {
            return ETranslationDocumentFormat.html;
        }

        public string ToBase64()
        {
            var segments = originalData.Select(item => $"<div>{item}</div>");
            var bytes = Encoding.Default.GetBytes(string.Join("", segments));
            var segmentBase64 = Convert.ToBase64String(bytes);

            return segmentBase64;
        }
    }
}
