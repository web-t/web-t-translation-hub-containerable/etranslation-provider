using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Interfaces.Converters;

namespace Tilde.TranslationProxyETranslation.Converters.File
{
    public class TxtConverter : IFileConverter
    {
        private readonly List<string> originalData;

        public bool Supported { get; private set; } = true;

        public TxtConverter(IEnumerable<string> data)
        {
            originalData = data.ToList();
        }

        public IEnumerable<string> FromBase64(string data)
        {
            var bytes = Convert.FromBase64String(data);
            var txt = Encoding.UTF8.GetString(bytes);

            var segments = txt.Split("\n").ToList();

            if (segments.Count == originalData.Count + 1 && string.IsNullOrEmpty(segments.Last()))
            {
                segments = segments.SkipLast(1).ToList();
            }

            if (segments.Count != originalData.Count)
            {
                throw new ArgumentException($"Response segments '{segments.Count}' does not match source segments '{originalData.Count}'");
            }

            return segments;
        }

        public ETranslationDocumentFormat GetCEFDocFormat()
        {
            return ETranslationDocumentFormat.txt;
        }

        public string ToBase64()
        {
            var normalizedText = originalData.Select(item => item.Replace("\n", ""));
            var bytes = Encoding.Default.GetBytes(string.Join("\n", normalizedText));
            var segmentBase64 = Convert.ToBase64String(bytes);

            return segmentBase64;
        }
    }
}
