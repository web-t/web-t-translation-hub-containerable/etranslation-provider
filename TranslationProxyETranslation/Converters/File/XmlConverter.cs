using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Interfaces.Converters;

namespace Tilde.TranslationProxyETranslation.Converters.File
{
    public class XmlConverter : IFileConverter
    {
        private const string XmlPreamble = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        private readonly XmlDocument document = new XmlDocument();

        private readonly IEnumerable<string> originalData;

        public bool Supported { get; private set; }

        public XmlConverter(IEnumerable<string> data)
        {
            originalData = data;

            try
            {
                var segments = originalData.Select(item => $"<segment>{item}</segment>");
                var xml = $"{XmlPreamble}<root>{string.Join("", segments)}</root>";

                document.LoadXml(xml);

                Supported = true;
            }
            catch (XmlException)
            {
                Supported = false;
            }
        }

        public string ToBase64()
        {
            var bytes = Encoding.Default.GetBytes(document.OuterXml);
            var segmentBase64 = Convert.ToBase64String(bytes);

            return segmentBase64;
        }

        public IEnumerable<string> FromBase64(string data)
        {
            var bytes = Convert.FromBase64String(data);
            var xmlDocument = Encoding.UTF8.GetString(bytes);

            var doc = new XmlDocument();
            doc.LoadXml(xmlDocument);
            var root = doc.DocumentElement;

            var segments = doc.SelectNodes("/root/segment")
                .Cast<XmlNode>()
                .Select(item => item.InnerXml)
                .Select(item => ConvertToMemoqXliff(item));

            return segments;
        }

        /// <summary>
        /// For File translation and CAT tool we need to preseve self-closing tag format
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private string ConvertToMemoqXliff(string xml)
        {
            return Regex.Replace(xml, @"<g id=""(\d+)""\s*/>", (match) =>
            {
                return $"<g id=\"{match.Groups[1]}\"></g>";
            }, RegexOptions.IgnoreCase);
        }

        public ETranslationDocumentFormat GetCEFDocFormat()
        {
            return ETranslationDocumentFormat.xml;
        }
    }
}
