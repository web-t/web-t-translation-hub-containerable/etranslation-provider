﻿namespace Tilde.TranslationProxyETranslation.Enums
{
    /// <summary>
    /// Redis Pub/Sub channel
    /// </summary>
    public enum RedisPubSubChannels
    {
        Tilde_TranslationProxy_eTranslation
    }
}
