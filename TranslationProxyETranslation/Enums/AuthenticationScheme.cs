﻿namespace Tilde.TranslationProxyETranslation.Enums
{
    public static class AuthenticationScheme
    {
        /// <summary>
        /// Authentication scheme for a basic authentication 
        /// </summary>
        public const string BasicAuthScheme = "basicauthscheme";
    }
}
