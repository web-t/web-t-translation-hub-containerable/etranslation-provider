﻿namespace Tilde.TranslationProxyETranslation.Enums
{
    /// <summary>
    /// ETransaltion document formats
    /// 
    /// Ref: https://ec.europa.eu/cefdigital/wiki/display/CEFDIGITAL/eTranslation+web+service+specification+list
    /// </summary>
    public enum ETranslationDocumentFormat
    {
        odt,
        ods,
        odp,
        odg,
        ott,
        ots,
        otp,
        otg,
        rtf,
        doc,
        docx,
        xls,
        xlsx,
        ppt,
        ppts,
        pdf,
        txt,
        htm,
        html,
        xhtml,
        xml,
        xlf,
        xliff,
        sdlxliff,
        tmx,
        rdf
    }
}
