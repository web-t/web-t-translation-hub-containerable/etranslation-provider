﻿using System;
using System.Collections.Generic;
using Tilde.TranslationProxyETranslation.Converters.File;
using Tilde.TranslationProxyETranslation.Interfaces.Converters;

namespace Tilde.TranslationProxyETranslation.Factories
{
    public class FileConverterFactory
    {
        public FileConverterFactory(

        )
        {

        }

        /// <summary>
        /// Creates compatible converter for specific data
        /// </summary>
        /// <param name="data"> Text segments </param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public IFileConverter Create(IEnumerable<string> data)
        {
            var xmlConverter = new XmlConverter(data);

            if (xmlConverter.Supported)
            {
                return xmlConverter;
            }

            var txtConverter = new TxtConverter(data);
            if (txtConverter.Supported)
            {
                return txtConverter;
            }

            /*var htmlConverter = new HtmlConverter(data);
            if (htmlConverter.Supported)
            {
                return htmlConverter;
            }*/

            throw new NotImplementedException("data format not recognized by File Converters");
        }
    }
}
