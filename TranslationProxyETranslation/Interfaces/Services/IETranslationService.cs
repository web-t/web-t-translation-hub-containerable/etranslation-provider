﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Tilde.TranslationProxyETranslation.Models.Configuration;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.EMW.Contracts.Models.Common.Translation;

namespace Tilde.TranslationProxyETranslation.Interfaces.Services
{
    public interface IETranslationService
    {
        /// <summary>
        /// TranslateSegment text
        /// </summary>
        /// <param name="text"></param>
        /// <param name="sourceLanguage"></param>
        /// <param name="targetLanguage"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        Task<Translation> Translate(List<string> text, string sourceLanguage, string targetLanguage, string domain, Authentication authentication);
        /// <summary>
        /// Process translation response from eTranslation callback
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="requestSecret"></param>
        /// <param name="segmentBase64"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public Task TranslateCallback(string requestId, string requestSecret, string segmentBase64, string errorCode, string errorMessage);
        /// <summary>
        /// Returns list of eTranslation language directions
        /// </summary>
        /// <returns></returns>
        public Task<LanguageDirectionsResponse> FetchDomains(Authentication authentication);
    }
}
