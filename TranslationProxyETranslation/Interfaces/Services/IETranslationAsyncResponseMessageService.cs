﻿namespace Tilde.TranslationProxyETranslation.Interfaces.Services
{
    public interface IETranslationAsyncResponseMessageService
    {
        /// <summary>
        /// Process redis message and forward messsage to eTranslation requester
        /// </summary>
        /// <param name="redisMessage"></param>
        public void OnRedisMessage(string redisMessage);
    }
}
