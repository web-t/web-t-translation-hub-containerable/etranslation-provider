﻿using System.Threading.Tasks;

namespace Tilde.TranslationProxyETranslation.Interfaces.Services
{
    public interface IETranslationAsyncResponseService
    {
        /// <summary>
        /// Register request and return task that completes when translation result is available
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="requestSecret"></param>
        /// <returns></returns>
        public Task<string> RegisterRequest(string requestId, string requestSecret);

        /// <summary>
        /// Register eTranslation response that will be passed to correct service instance that was requesting transaltion
        /// </summary>
        /// <param name="requestId"></param>
        /// <param name="requestSecret"></param>
        /// <param name="contentsBase64"></param>
        /// <param name="errorCode"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public Task RegisterResponse(string requestId, string requestSecret, string contentsBase64, string errorCode, string errorMessage);
    }
}
