﻿using System.Collections.Generic;
using Tilde.TranslationProxyETranslation.Enums;

namespace Tilde.TranslationProxyETranslation.Interfaces.Converters
{
    public interface IFileConverter
    {
        /// <summary>
        /// File format supports data
        /// </summary>
        /// <returns></returns>
        public bool Supported { get; }

        /// <summary>
        /// Convert file to base64
        /// </summary>
        /// <returns></returns>
        public string ToBase64();

        /// <summary>
        /// Convert file from base64
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IEnumerable<string> FromBase64(string data);

        /// <summary>
        /// Get file format extension for ETranslation
        /// </summary>
        /// <returns></returns>
        public ETranslationDocumentFormat GetCEFDocFormat();
    }
}
