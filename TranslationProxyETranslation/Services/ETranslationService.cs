﻿using EnumsNET;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;
using RestSharp.Authenticators.Digest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.EMW.Contracts.Models.Common.LanguageDirection;
using Tilde.EMW.Contracts.Models.Common.Translation;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Exceptions.Authentication;
using Tilde.TranslationProxyETranslation.Exceptions.ETranslation;
using Tilde.TranslationProxyETranslation.Factories;
using Tilde.TranslationProxyETranslation.Interfaces.Services;
using Tilde.TranslationProxyETranslation.Models.Configuration;
using Tilde.TranslationProxyETranslation.Models.DTO.ETranslation;
using Tilde.TranslationProxyETranslation.Models.Translators.ETranslation;


namespace Tilde.TranslationProxyETranslation.Services
{
    public class ETranslationService : IETranslationService
    {
        private readonly Uri translationUri = new("https://webgate.ec.europa.eu/etranslation/si/translate");
        private readonly Uri systemListUri = new("https://webgate.ec.europa.eu/etranslation/si/get-domains");

        private readonly ILogger _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ConfigurationServices _serviceConfiguration;
        private readonly ConfigurationSettings _settingsConfiguration;
        private readonly IETranslationAsyncResponseService _eTranslationAsyncResponseService;
        private readonly FileConverterFactory _fileConverterFactory;

        public ETranslationService(
            ILogger<ETranslationService> logger,
            IHttpClientFactory httpClientFactory,
            IOptions<ConfigurationServices> serviceConfiguration,
            IOptions<ConfigurationSettings> settingsConfiguration,
            IETranslationAsyncResponseService eTranslationAsyncResponseService,
            FileConverterFactory fileConverterFactory
        )
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _serviceConfiguration = serviceConfiguration.Value;
            _settingsConfiguration = settingsConfiguration.Value;
            _eTranslationAsyncResponseService = eTranslationAsyncResponseService;
            _fileConverterFactory = fileConverterFactory;
        }

        /// <summary>
        /// Translate callback for registering eTranslation response
        /// </summary>
        /// <param name="requestId"> Request unique identifier </param>
        /// <param name="requestSecret"> Request secret </param>
        /// <param name="segmentBase64"> Segment content as base64 string </param>
        /// <param name="errorCode"> Error code </param>
        /// <param name="errorMessage"> Error message </param>
        /// <returns></returns>
        public async Task TranslateCallback(string requestId, string requestSecret, string segmentBase64, string errorCode, string errorMessage)
        {
            await _eTranslationAsyncResponseService.RegisterResponse(requestId, requestSecret, segmentBase64, errorCode, errorMessage);
        }

        /// <summary>
        /// Translate text segments
        /// </summary>
        /// <param name="text"> Texts </param>
        /// <param name="sourceLanguage"> Source language </param>
        /// <param name="targetLanguage"> Target language </param>
        /// <param name="domain"> Domain </param>
        /// <param name="authentication"> ETranslation username and password </param>
        /// <returns></returns>

        public async Task<Translation> Translate(List<string> text, string sourceLanguage, string targetLanguage, string domain, Authentication authentication)
        {
            IEnumerable<string> translations = text;

            var anyText = text
                .Select(item => !string.IsNullOrWhiteSpace(item))
                .Any();

            if (anyText)
            {
                translations = await TranslateSegment(text, sourceLanguage, targetLanguage, domain, authentication);
            }

            var result = new Translation()
            {
                Domain = domain,
                Translations = translations
                    .Select(item =>
                    {
                        return new TranslationItem()
                        {
                            Translation = item
                        };
                    }).ToList()
            };

            return result;
        }

        /// <summary>
        /// Send translation request to ETranslation service
        /// </summary>
        /// <param name="segments"> Texts </param>
        /// <param name="sourceLanguage"> Source language </param>
        /// <param name="targetLanguage"> Target language </param>
        /// <param name="domain"> Domain </param>
        /// <param name="authentication"> ETranslation username and password </param>
        /// <returns></returns>
        /// <exception cref="LanguageDirectionNotFoundException"></exception>
        /// <exception cref="UserIsNotAuthenticatedException"></exception>
        /// <exception cref="ETranslationException"></exception>
        public async Task<IEnumerable<string>> TranslateSegment(IEnumerable<string> segments, string sourceLanguage, string targetLanguage, string domain, Authentication authentication)
        {
            var startTime = DateTime.Now;

            var converter = _fileConverterFactory.Create(segments);

            var uriBuilder = new UriBuilder(_settingsConfiguration.PublicUri);
            uriBuilder.Path = $"{uriBuilder.Path}/translate/text/e-translation";

            var httpDestination = uriBuilder.Uri.ToString();


            // Request secret protects user translations so that translations for users cannot be easily manipulated.
            // Users should receive response from eTranslation, not from bad actor
            var requestSecret = Guid.NewGuid();

            var requestData = new Models.Translators.ETranslation.TranslationRequest()
            {
                ExternalReference = $"{requestSecret}",
                CallerInfo = new CallerInformation()
                {
                    Application = authentication.Username
                },
                SourceLanguage = sourceLanguage,
                TargetLanguages = new List<string>()
                {
                    targetLanguage
                },
                Domain = domain,
                DocumentToTranslateBase64 = new DocumentBase64()
                {
                    Content = converter.ToBase64(),
                    Format = converter.GetCEFDocFormat().AsString(EnumFormat.Name)
                },
                ErrorCallback = $"{httpDestination}/error",
                Destinations = new Destinations()
                {
                    HTTPDestinations = new List<string>() {
                        httpDestination
                    },
                    EmailDestinations = _serviceConfiguration.ETranslation.EmailRecipients
                }
            };


            var httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = translationUri;

            var options = new RestClientOptions();
            options.Authenticator = new DigestAuthenticator(authentication.Username, authentication.Password);

            var client = new RestClient(httpClient, options) {};

            var request = new RestRequest("", Method.Post)
                .AddJsonBody(requestData);

            var response = await client.ExecuteAsync<int>(request);
            var responseCode = response.Data;

            _logger.LogDebug("eTranslation request: Request id '{RequestId}' Destination: {Destination}, Converter: '{Converter}', Segment: '{Segment}', encoded data: {Data}",
                responseCode,
                httpDestination,
                converter.GetCEFDocFormat().AsString(EnumFormat.Name),
                segments,
                requestData.DocumentToTranslateBase64.Content
            );
            switch (responseCode)
            {
                case (int)ETranslationError.SourceLanguageIsInvalid:
                    throw new LanguageDirectionNotFoundException(sourceLanguage);
                case (int)ETranslationError.TargetLanguageIsInvalid:
                    throw new LanguageDirectionNotFoundException(targetLanguage);
                case (int)ETranslationError.LanguagePairIsNotSupported:
                    throw new LanguageDirectionNotFoundException(domain, sourceLanguage, targetLanguage);
                case 0:
                    throw new UserIsNotAuthenticatedException();
                case < 0:
                    throw new ETranslationException(responseCode);
            }

            var resultBase64 = await _eTranslationAsyncResponseService.RegisterRequest(responseCode.ToString(), requestSecret.ToString());

            _logger.LogInformation("eTranslation request '{Request}' finished in '{Duration}'", responseCode.ToString(), DateTime.Now - startTime);

            var result = converter.FromBase64(resultBase64);

            _logger.LogDebug("eTranslation response: '{RequestId}'. Plain text response: '{Response}'. Original response: '{OriginalResponse}'",
                responseCode.ToString(),
                result,
                resultBase64
            );

            return result;
        }

        /// <summary>
        /// Get language directions from ETranslation service
        /// </summary>
        /// <param name="authentication"> ETranslation username and password </param>
        /// <returns></returns>
        /// <exception cref="UserIsNotAuthenticatedException"></exception>
        /// <exception cref="Exception"></exception>
        public async Task<LanguageDirectionsResponse> FetchDomains(Authentication authentication)
        {
            var httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = systemListUri;

            var options = new RestClientOptions();
            options.Authenticator = new DigestAuthenticator(authentication.Username, authentication.Password);

            var client = new RestClient(httpClient, options) {};

            var request = new RestRequest("", Method.Get);

            var response = await client.ExecuteAsync<Dictionary<string, Domain>>(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                throw new UserIsNotAuthenticatedException(response.Content);
            }
            else if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"ETranslation system load failed with status code: {response.StatusCode}");
            }

            var languageDirections = ConvertToLanguageDirectionsResponse(response.Data);

            return languageDirections;
        }

        /// <summary>
        /// Convert ETranslation response to service language directions model
        /// </summary>
        /// <param name="serviceResponse"> ETranslation service response </param>
        /// <returns></returns>
        private LanguageDirectionsResponse ConvertToLanguageDirectionsResponse(Dictionary<string, Domain> serviceResponse)
        {
            LanguageDirectionsResponse languageDirectionsResponse = new LanguageDirectionsResponse();
            List<LanguageDirection> languageDirections = new List<LanguageDirection>();
            foreach (var langDirection in serviceResponse)
            {
                var domainModel = langDirection.Value;

                foreach (var languagePair in domainModel.LanguagePairs)
                {
                    string[] languages = languagePair.Split('-');

                    languageDirections.Add(
                        new LanguageDirection()
                        {
                            Domain = langDirection.Key,
                            ProviderType = EMW.Contracts.Enums.Provider.Type.ETranslation,
                            SourceLanguage = languages[0],
                            TargetLanguage = languages[1]
                        }
                    );
                }
            }

            languageDirectionsResponse.LanguageDirections = languageDirections;
            return languageDirectionsResponse;
        }
    }
}
