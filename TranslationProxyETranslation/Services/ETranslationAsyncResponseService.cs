﻿using EnumsNET;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tilde.EMW.Contracts.Exceptions.TranslationSystem;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Exceptions.ETranslation;
using Tilde.TranslationProxyETranslation.Interfaces.Services;
using Tilde.TranslationProxyETranslation.Models.Configuration;
using Tilde.TranslationProxyETranslation.Models.Translators.ETranslation;

namespace Tilde.TranslationProxyETranslation.Services
{
    public class ETranslationAsyncResponseService : IETranslationAsyncResponseService, IETranslationAsyncResponseMessageService
    {
        private readonly ConcurrentDictionary<KeyValuePair<string, string>, TaskCompletionSource<string>> requests = new();
        private readonly ConfigurationServices _serviceConfiguration;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly ILogger _logger;

        public ETranslationAsyncResponseService(
            ILogger<ETranslationAsyncResponseService> logger,
            IConnectionMultiplexer connectionMultiplexer,
            IOptions<ConfigurationServices> serviceConfiguration
        )
        {
            _connectionMultiplexer = connectionMultiplexer;
            _logger = logger;
            _serviceConfiguration = serviceConfiguration.Value;
        }

        /// <summary>
        /// Register eTranslation request
        /// </summary>
        /// <param name="requestId"> Request unique identifier </param>
        /// <param name="requestSecret"> Request secret </param>
        /// <returns></returns>
        /// <exception cref="ETranslationRequestConflictException"></exception>
        public Task<string> RegisterRequest(string requestId, string requestSecret)
        {
            _logger.LogDebug("Register eTransaltion request '{RequestId}'", requestId);
            var tcs = new TaskCompletionSource<string>();

            var storageKey = new KeyValuePair<string, string>(requestId, requestSecret);

            var cts = new CancellationTokenSource(_serviceConfiguration.ETranslation.Timeout);
            cts.Token.Register(() =>
            {
                _logger.LogDebug("Mark eTranslation request: {RequestId} as expired with timeout: {Timeout}", requestId, _serviceConfiguration.ETranslation.Timeout);
                tcs.TrySetException(new TranslationTimeoutException(_serviceConfiguration.ETranslation.Timeout));
                requests.TryRemove(storageKey, out var value);
            });

            if (!requests.TryAdd(storageKey, tcs))
            {
                throw new ETranslationRequestConflictException(requestId);
            }
            return tcs.Task;
        }

        /// <summary>
        /// Register eTranslation response
        /// </summary>
        /// <param name="requestId"> Request unique identifier </param>
        /// <param name="requestSecret"> Request secret </param>
        /// <param name="contentsBase64"> Request content as base64 string </param>
        /// <param name="errorCode"> Error code </param>
        /// <param name="errorMessage"> Error message </param>
        /// <returns></returns>
        public async Task RegisterResponse(string requestId, string requestSecret, string contentsBase64, string errorCode, string errorMessage)
        {
            _logger.LogDebug("Register eTransaltion response '{RequestId}', '{ContentsBase64}', '{Error}', '{ErrorMessage}'", requestId, contentsBase64, errorCode, errorMessage);

            var subscriber = _connectionMultiplexer.GetSubscriber();

            var redisMessage = EncodeRedisMessage(requestId, requestSecret, contentsBase64, errorCode, errorMessage);
            await subscriber.PublishAsync(RedisPubSubChannels.Tilde_TranslationProxy_eTranslation.AsString(EnumFormat.Name), redisMessage);
        }

        /// <summary>
        /// Set redis message
        /// </summary>
        /// <param name="redisMessage"> Redis message </param>
        public void OnRedisMessage(string redisMessage)
        {
            var message = DecodeRedisMessage(redisMessage);

            var storageKey = new KeyValuePair<string, string>(message.RequestId, message.RequestSecret);

            if (requests.TryRemove(storageKey, out var tcs))
            {
                if (message.ErrorCode.HasValue)
                {
                    if (message.ErrorCode.Value == (int)ETranslationError.ConcurrencyQuotaExceeded)
                    {
                        tcs.TrySetException(new ETranslationConcurrencyQuotaExceeded());
                    }
                    else
                    {
                        tcs.TrySetException(new ETranslationException(message.RequestId, message.ErrorCode.Value, message.ErrorMessage));
                    }
                }
                else
                {
                    tcs.TrySetResult(message.ContentsBase64);
                }
            }
            else
            {
                _logger.LogDebug("Message with requestId: {RequestId} not known or is expired. Error code: {ErrorCode}, Error message: {ErrorMessage}. Message: {Message}",
                    message.RequestId, message.ErrorCode, message.ErrorMessage, message.ContentsBase64);
            }
        }

        /// <summary>
        /// Encode redis message
        /// </summary>
        /// <param name="requestId"> Request unique identifier </param>
        /// <param name="requestSecret"> Request secret </param>
        /// <param name="contentsBase64"> Request content as base64 string </param>
        /// <param name="errorCode"> Error code </param>
        /// <param name="errorMessage"> Error message </param>
        /// <returns></returns>
        private static string EncodeRedisMessage(string requestId, string requestSecret, string contentsBase64, string errorCode, string errorMessage)
        {
            var message = $"{requestId}:{requestSecret}:{contentsBase64 ?? ""}:{errorCode ?? ""}:{errorMessage ?? ""}";
            return message;
        }

        /// <summary>
        /// Decode redis message
        /// </summary>
        /// <param name="redisMessage"> Redis message </param>
        /// <returns></returns>
        private static RedisMessage DecodeRedisMessage(string redisMessage)
        {
            var message = redisMessage.Split(":");
            int? errorCode = null;
            if (int.TryParse(message[3], out int val))
            {
                errorCode = val;
            }

            var response = new RedisMessage()
            {
                RequestId = message[0],
                RequestSecret = message[1],
                ContentsBase64 = message[2],
                ErrorCode = errorCode,
                ErrorMessage = message[4]
            };

            return response;
        }
    }
}
