﻿using EnumsNET;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System.Threading;
using System.Threading.Tasks;
using Tilde.TranslationProxyETranslation.Enums;
using Tilde.TranslationProxyETranslation.Interfaces.Services;

namespace Tilde.TranslationProxyETranslation.Services
{
    public class RedisSubscriptionBackgroundService : IHostedService
    {
        private readonly ILogger _logger;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IETranslationAsyncResponseMessageService _eTransaltionMessageService;

        private ISubscriber subscriber;

        public RedisSubscriptionBackgroundService(
            ILogger<RedisSubscriptionBackgroundService> logger,
            IConnectionMultiplexer connectionMultiplexer,
            IETranslationAsyncResponseMessageService eTransaltionMessageService
        )
        {
            _logger = logger;
            _connectionMultiplexer = connectionMultiplexer;
            _eTransaltionMessageService = eTransaltionMessageService;
        }

        /// <summary>
        /// Subscribe to ETranslation redis channel
        /// </summary>
        /// <param name="cancellationToken"> Cancellation token </param>
        /// <returns></returns>
        public Task StartAsync(CancellationToken cancellationToken)
        {
            subscriber = _connectionMultiplexer.GetSubscriber();

            subscriber.Subscribe(
                RedisPubSubChannels.Tilde_TranslationProxy_eTranslation.AsString(EnumFormat.Name),
                (channel, message) =>
                {
                    var redisMessage = (string)message;
                    _logger.LogDebug("Received eTranslation message: {Message}", redisMessage);
                    _eTransaltionMessageService.OnRedisMessage(redisMessage);
                }
            );

            return Task.CompletedTask;
        }

        /// <summary>
        /// Unsubscribe from ETranslation redis channel
        /// </summary>
        /// <param name="cancellationToken"> Cancellation token </param>
        /// <returns></returns>
        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await subscriber.UnsubscribeAllAsync();
        }
    }
}
