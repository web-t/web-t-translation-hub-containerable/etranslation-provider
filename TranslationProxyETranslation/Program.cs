using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using StackExchange.Redis;
using Tilde.TranslationProxyETranslation.Extensions;
using Tilde.TranslationProxyETranslation.Factories;
using Tilde.TranslationProxyETranslation.Interfaces.Services;
using Tilde.TranslationProxyETranslation.Models.Configuration;
using Tilde.TranslationProxyETranslation.Models.Mappings;
using Tilde.TranslationProxyETranslation.Services;

var builder = WebApplication.CreateBuilder(args);

var configurationServices = builder.Configuration.GetSection("Services").Get<ConfigurationServices>();

builder.Host
    .UseSerilog((ctx, config) =>
    {
        config
            .Enrich.FromLogContext()
            .WriteTo.Debug()
            .WriteTo.Console()
            .ReadFrom.Configuration(builder.Configuration);
    });

builder.Services.AddHttpClient();

builder.Services.AddMemoryCache();

builder.Services.AddCorsPolicies();

builder.Services.Configure<ConfigurationServices>(builder.Configuration.GetSection("Services"));
builder.Services.Configure<ConfigurationSettings>(builder.Configuration.GetSection("Configuration"));
builder.Services.Configure<Authentication>(builder.Configuration.GetSection("Authentication"));

builder.Services.AddHealthChecks();

builder.Services.AddControllers();

var configuration = builder.Configuration.GetSection("Configuration").Get<ConfigurationSettings>();

builder.Services.AddDocumentation(configuration);

var mappingConfig = new MapperConfiguration(config =>
{
    config.AddProfile(new MappingProfile());
});

builder.Services.AddSingleton(mappingConfig.CreateMapper());
builder.Services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(configurationServices.Redis.Uri));

builder.Services.AddSingleton<IETranslationService, ETranslationService>();
builder.Services.AddHostedService<RedisSubscriptionBackgroundService>();

builder.Services.AddSingleton<FileConverterFactory>();
builder.Services.AddSingleton<ETranslationAsyncResponseService>();
builder.Services.AddSingleton<IETranslationAsyncResponseMessageService>(provider => provider.GetRequiredService<ETranslationAsyncResponseService>());
builder.Services.AddSingleton<IETranslationAsyncResponseService>(provider => provider.GetRequiredService<ETranslationAsyncResponseService>());

builder.Services.AddHttpContextAccessor();

var app = builder.Build();

app.UseDocumentation(configuration);

app.UseRouting();

app.UseAuthorization();
#if DEBUG
app.UseCorsPolicies();
#endif

app.MapControllers();

// Startup probe / readyness probe
app.MapHealthChecks("/health/ready", new HealthCheckOptions()
{

});

// Liveness 
app.MapHealthChecks("/health/live", new HealthCheckOptions()
{

});

app.Run();