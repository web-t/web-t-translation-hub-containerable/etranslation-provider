using System;
using System.Text;
using Tilde.TranslationProxyETranslation.Exceptions.Authentication;
using Tilde.TranslationProxyETranslation.Models.Configuration;
namespace Tilde.TranslationProxyETranslation.Helpers
{
    public static class BasicAuthenticationHelper
    {
        /// <summary>
        /// Extract username and password from basic auth header
        /// </summary>
        /// <param name="authorizationHeader"> Request authorization header </param>
        /// <returns></returns>
        /// <exception cref="UserIsNotAuthenticatedException"></exception>
        public static Authentication GetCredentials(string authorizationHeader)
        {
            if (authorizationHeader != null && authorizationHeader.StartsWith("Basic"))
            {
                string encodedCredentials = authorizationHeader.Substring("Basic ".Length).Trim();
                Encoding encoding = Encoding.GetEncoding("iso-8859-1");
                string credentials = encoding.GetString(Convert.FromBase64String(encodedCredentials));
                int indexOfSeparator = credentials.IndexOf(':');
                if (indexOfSeparator < 0)
                {
                    throw new UserIsNotAuthenticatedException();
                }

                var credentialsModel = new Authentication()
                {
                    Username = credentials.Substring(0, indexOfSeparator),
                    Password = credentials.Substring(indexOfSeparator + 1)
                };

                return credentialsModel;
            }

            throw new UserIsNotAuthenticatedException();
        }
    }
}